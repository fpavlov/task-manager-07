package ru.t1.fpavlov.tm;

import ru.t1.fpavlov.tm.model.Command;

import java.util.Scanner;

import static ru.t1.fpavlov.tm.constant.TerminalConst.*;
import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.util.FormatUtil.bytesToHumanReadable;

public class Application {

    public static void main(String[] args) {
        run(args);
    }

    private static void displayWelcomeText() {
        System.out.println("**WELCOME TO TASK MANAGER**\n");
    }

    private static void displayHelp() {
        System.out.format(
                "Commands:%n" +
                        "\t" + Command.VERSION + "%n" +
                        "\t" + Command.ABOUT + "%n" +
                        "\t" + Command.HELP + "%n" +
                        "\t" + Command.INFO + "%n" +
                        "\t" + Command.EXIT + "%n"
        );
    }

    private static void displayAbout() {
        System.out.println("\tPavlov Philipp");
        System.out.println("\tfpavlov@t1-consulting.ru");
    }

    private static void displayVersion() {
        System.out.println("\t1.7.0");
    }

    private static void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            listenerArgument(args[0]);
            return;
        }

        interactiveCommandProcessing();
    }

    private static void interactiveCommandProcessing() {
        String param;
        final Scanner scanner = new Scanner(System.in);

        displayWelcomeText();

        while (true) {
            System.out.println("-- Please enter a command --");
            param = scanner.nextLine();
            listenerCommand(param);
        }
    }

    private static void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_INFO:
                displaySystemInfo();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                incorrectCommand();
        }
    }

    private static void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                displayAbout();
                break;
            case CMD_SHORT_VERSION:
                displayVersion();
                break;
            case CMD_SHORT_INFO:
                displaySystemInfo();
                break;
            default:
                incorrectArgument();
        }

        System.exit(0);
    }

    private static void incorrectCommand() {
        System.out.println("\tIncorrect Command");
    }

    private static void incorrectArgument() {
        System.out.println("\tIncorrect Argument");
    }

    private static void quite() {
        System.exit(0);
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void displaySystemInfo() {
        Runtime runtime = Runtime.getRuntime();

        System.out.format(
                "System info:%n" +
                        "\t - Available processors: %s%n" +
                        "\t - Free memory: %s%n" +
                        "\t - Maximum memory: %s%n" +
                        "\t - Total memory: %s%n" +
                        "\t - Used memory: %s%n",
                runtime.availableProcessors(),
                bytesToHumanReadable(runtime.freeMemory()),
                bytesToHumanReadable(runtime.maxMemory()),
                bytesToHumanReadable(runtime.totalMemory()),
                bytesToHumanReadable(runtime.totalMemory() - runtime.freeMemory())
        );
    }

}