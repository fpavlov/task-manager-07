package ru.t1.fpavlov.tm.constant;

/**
 * Created by fpavlov on 26.09.2021.
 */
public final class ArgumentConst {

    public static final String CMD_SHORT_HELP = "-h";

    public static final String CMD_SHORT_VERSION = "-v";

    public static final String CMD_SHORT_ABOUT = "-a";

    public static final String CMD_SHORT_INFO = "-i";

}
