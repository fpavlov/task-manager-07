package ru.t1.fpavlov.tm.constant;

/*
 * Created by fpavlov on 19.09.2021.
 */
public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_INFO = "info";

}
