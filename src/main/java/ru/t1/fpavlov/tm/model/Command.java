package ru.t1.fpavlov.tm.model;

import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;

/*
 * Created by fpavlov on 28.09.2021.
 */
public class Command {

    public static Command ABOUT = new Command(
            CMD_ABOUT,
            CMD_SHORT_ABOUT,
            "Display developer info"
    );

    public static Command HELP = new Command(
            CMD_HELP,
            CMD_SHORT_HELP,
            "Display list of terminal command"
    );

    public static Command VERSION = new Command(
            CMD_VERSION,
            CMD_SHORT_VERSION,
            "Display program version"
    );

    public static Command EXIT = new Command(
            CMD_EXIT,
            null,
            "Quite"
    );

    public static Command INFO = new Command(
            CMD_INFO,
            CMD_SHORT_INFO,
            "Show available resource of system"
    );

    private String name;

    private String argument;

    private String description;

    public Command() {
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public Command(final String name, final String description) {
        this.name = name;
        this.argument = null;
        this.description = description;
    }

    @Override
    public String toString() {
        String formatMessage = "";

        if (this.name != null && !this.name.isEmpty()) {
            formatMessage += this.name;
        }

        if (this.argument != null && !this.argument.isEmpty()) {
            formatMessage += "," + this.argument;
        }

        if (this.description != null && !this.description.isEmpty()) {
            formatMessage += " - " + this.description;
        }

        return formatMessage;
    }

}
